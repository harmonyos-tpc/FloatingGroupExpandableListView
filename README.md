## FloatingGroupExpandableListView

## Introduction:
FloatingGroupExpandableListView is an open source library that provides a floating group list view using custom components with expand & collapse behavior.

## Usage Instructions:
1. Prepare group items with following code:
```java

		private void getGroupItems() {
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_cupcake));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_donut));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_eclair));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_froyo));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_gingerbread));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_honeycomb));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_ice_cream_sandwich));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_jelly_bean));
            mGroupNameItem.add(ResUtil.getString(this, ResourceTable.String_item_kitkat));
            mFinalGroupNameItem.addAll(mGroupNameItem);
		}
```

2. Prepare group items images with following code:
```java

		private void getGroupIcons() {
            mGroupImageItem.add(ResourceTable.Media_cupcake);
            mGroupImageItem.add(ResourceTable.Media_donut);
            mGroupImageItem.add(ResourceTable.Media_eclair);
            mGroupImageItem.add(ResourceTable.Media_froyo);
            mGroupImageItem.add(ResourceTable.Media_gingerbread);
            mGroupImageItem.add(ResourceTable.Media_honeycomb);
            mGroupImageItem.add(ResourceTable.Media_ice_cream_sandwich);
            mGroupImageItem.add(ResourceTable.Media_jelly_bean);
            mGroupImageItem.add(ResourceTable.Media_kitkat);
    	}
```

3. Prepare adapter to show your list items:
```java
		ExpandableListAdapter expandableListAdapter = new  ExpandableListAdapter<String>
	              (this, com.diegocarloslima.fgelv.lib.ResourceTable.Layout_sample_ability_list_group_item,
	              mGroupNameItem, mGroupImageItem) {
	        	// Code here for binding group & child item
	        }
	        mGroupContainer. setAdapter(expandableListAdapter);
```

4. ExpandableListViewAdapter api's:
```
        ExpandableListAdapter expandableListAdapter = new ExpandableListAdapter<String>(this, ResourceTable.Layout_sample_ability_list_group_item,
         mGroupNameItem, mGroupImageItem) {
                    @Override
                    protected void bind(ViewHolder holder, String text, Integer image, int position) {

                    }
              }
			  
            a.  using holder can set the text
               holder.setText(viewId, String text, color, textSize)
               int  viewId - Provide the text component id for which text field needs to display the content.
               String text - text content need to display
               Color color - color of the text
               int textsize - text size for display.

            b. using holder can set the group image
                  holder.setGroupImage(viewId, imageRedId, shape, scaleMode, groupImageColor)
                  int viewId - Provide the group imageView component id for which group image can be set.
                  Integer imageResId - provide the image id
                  int shape - group image shape
                  Image.ScaleMode scaleMode - scale mode for the image
                  int groupImageColor - groupImageColor which need to set for background of the image

             c. using holder can set the child image
                 holder.setChildImage(viewId, imageRedId, width, height, margin)
                 int viewId - Provide the group imageView component id for which child image can be set.
                 Integer imageResId - provide the image id
                 int width - width of the child image
                 int height - height of the child image
                 margin - margin of the child image.

             d.  using holder can set the group item background
                 holder.setGroupItemBackground(int groupItemLayoutId, int groupItemColor)
                 int groupItemLayoutId - group item Dependent layout id
                 int groupItemColor -color for layout container, group item background

             e.  using holder can set the group item divider
                 holder.setGroupItemDivider(int dividerId, int color)
                 int dividerId - divider Dependent layout id
                 int color - divider color
```

5. Child items adding and removal
```
        when click on expandable list adpater group item
        expandableListAdapter.setOnItemClickListener(new ExpandableListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Component component, int position) {
                      // logic for removal of child items when collapsed  based on need
                         removeChildItems(actualItemPosition, clickedItem);
                       //logic for adding childitems when expanded based on need
                         addChildItems(actualItemPosition, clickedItem);
                    }
                });
```				

## Installation instructions:
```
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])

Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
implementation 'io.openharmony.tpc.thirdlib:FloatingGroupExpandableListView:1.0.1'
```

## License

    Copyright 2013 Diego Carlos Lima

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
