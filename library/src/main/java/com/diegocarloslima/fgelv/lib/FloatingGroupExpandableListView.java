/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.diegocarloslima.fgelv.lib;

import ohos.agp.components.AttrSet;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

public class FloatingGroupExpandableListView extends ListContainer {
    private static final String TAG = FloatingGroupExpandableListView.class.getSimpleName();

    private ExpandableListAdapter mAdapter;

    private ScrolledListener mOnScrollListener;

    public FloatingGroupExpandableListView(Context context) {
        super(context);
        init();
    }

    public FloatingGroupExpandableListView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public FloatingGroupExpandableListView(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs, "");
        init();
    }

    private void init() {
        super.setScrolledListener(new ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                if (mOnScrollListener != null) {
                    mOnScrollListener.onContentScrolled(component, i, i1, i2, i3);
                }
            }
        });
    }

    @Override
    public void setItemProvider(BaseItemProvider itemProvider) {
        if (itemProvider instanceof ExpandableListAdapter) {
            setAdapter((ExpandableListAdapter) itemProvider);
        }
    }

    public void setAdapter(ExpandableListAdapter adapter) {
        super.setItemProvider(adapter);
        mAdapter = adapter;
    }
}
